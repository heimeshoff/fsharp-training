# Fsharp Training

## Prerequisites
This training guides you through the use of F#, no upfront knowledge of the language or functional programming pattern required.
Please install the F# compiler and an F#-friendly editor such as Visual Studio Code using the instructions at fsharp.org or http://ionide.io

### F# Setup with VS Code
* Install VS Code
* Install VS Code extensions: “Ionide-fsharp”, “Ionide-paket”
* Follow instructions on [Ionide-fsharp page](http://ionide.io/#20150804gettingstarted)

## Program
Follow the Fsharp-script files (*.fsx) in alphanumerical order, or be a rebel and do whatever you want. 